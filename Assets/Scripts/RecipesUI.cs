﻿using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName ="Recipe", menuName = "ScriptableObjects/RecipeScriptableObject", order =1)]

public class RecipesUI : ScriptableObject
{
    public string recipeName;
    public Sprite objectToCreate;
    public int level;
    public int amount;
    public bool unlocked;
    public bool readyToCraft;
    public List<ItemNeeded> ListOfItemNeeded = new List<ItemNeeded>();
    [System.Serializable]
    public class ItemNeeded
    {
        public Item Item;
        public int ItemAmountNeeded;
    }
}
