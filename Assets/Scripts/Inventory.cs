﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{
    public static Inventory inventoryInstance;
    public List<RecipesUI.ItemNeeded> selectedCraftList = new List<RecipesUI.ItemNeeded>();
    public RecipeButton selectedCraft;
    public Button craftButton;
    public List<Button> allRecipesButton = new List<Button>();
    public List<CraftNeedUI> craftUI = new List<CraftNeedUI>();
    public List<Item> allItems = new List<Item>();
    public int level = 4;
    public Text levelTxt;
    [Header("Animation")]
    public GameObject anim;
    public Image animImg;
    [System.Serializable]
    public class CraftNeedUI
    {
        public Image ItemNeeded;
        public Text ItemAmountNeeded;
    }
    private void Awake()
    {
        inventoryInstance = this;
        if (selectedCraft == null)
        {
            craftButton.interactable = false;
        }
    }

    private void Update()
    {
        if (EventSystem.current != null && EventSystem.current.currentSelectedGameObject == null)
        {
            if (selectedCraft != null)
            {
                EventSystem.current.SetSelectedGameObject(selectedCraft.gameObject);
            }
            else
            {
                EventSystem.current.SetSelectedGameObject(allRecipesButton[0].gameObject);
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }

        LevelUp();
    }
    public void ShowCraftRequired(List<RecipesUI.ItemNeeded> itemNeeded)
    {
        for (int i = 0; i < craftUI.Count; i++)
        {
            if (itemNeeded[i].Item != null)
            {

                craftUI[i].ItemNeeded.sprite = itemNeeded[i].Item.itemImg;
                if (itemNeeded[i].Item.amount >= itemNeeded[i].ItemAmountNeeded)
                {
                    craftUI[i].ItemNeeded.color = Color.white;
                    craftUI[i].ItemAmountNeeded.text = "(" + itemNeeded[i].Item.amount + "/" + itemNeeded[i].ItemAmountNeeded + ")";
                }
                else
                {
                    craftUI[i].ItemNeeded.color = Color.red;
                    craftUI[i].ItemAmountNeeded.text = "<color=red>(" + itemNeeded[i].Item.amount + "/" + itemNeeded[i].ItemAmountNeeded + ")</color>";
                }
            }
            else
            {
                craftUI[i].ItemNeeded.color = Color.clear;
                craftUI[i].ItemAmountNeeded.text = "( / )";
            }

        }
    }

    public void Craft()
    {
        if (selectedCraft.recipe.readyToCraft)
        {
            selectedCraft.recipe.amount += 1;
            ShowCraftedAnimation();
            for (int i = 0; i < selectedCraftList.Count; i++)
            {
                if (selectedCraftList[i].Item != null)
                {
                    selectedCraftList[i].Item.amount -= selectedCraftList[i].ItemAmountNeeded;
                }
            }
            UpdateButtons();
            ShowCraftRequired(selectedCraftList);
        }
        else
        {
            Debug.Log("No tienes los items necesarios para fabricar " + selectedCraft.recipe.recipeName);
        }
    }

    public void UpdateButtons()
    {
        for (int i = 0; i < allRecipesButton.Count; i++)
        {
            allRecipesButton[i].GetComponent<RecipeButton>().InitialiceButton();
        }
    }

    public void ShowCraftedAnimation()
    {
        anim.SetActive(true);
        animImg.sprite = selectedCraft.recipe.objectToCreate;
        anim.GetComponent<Animation>().Play();
    }

    public void ToogleCraftButton()
    {
        craftButton.interactable = !craftButton.interactable;
    }
    public void LevelUp()
    {
        if (Input.GetKeyDown(KeyCode.L))
        {
            level++;
            levelTxt.text = "LEVEL: " + level;
            UpdateButtons();
        }
        if (Input.GetKeyDown(KeyCode.F))
        {
            for (int i = 0; i < allItems.Count; i++)
            {
                allItems[i].amount += 5;
                ShowCraftRequired(selectedCraftList);
                UpdateButtons();
            }
        }
    }
}
