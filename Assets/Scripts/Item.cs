﻿using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "Item", menuName = "ScriptableObjects/ItemScriptableObject", order = 2)]

public class Item : ScriptableObject
{
    public Sprite itemImg;
    public int amount;
    public int originalAmount = 30;

    private void OnEnable()
    {
        amount = originalAmount;
    }
}
