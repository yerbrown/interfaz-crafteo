﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Animacioncrafteo : MonoBehaviour
{
    public GameObject eventSystem;
    public void ToogleButton()
    {
        if (eventSystem.activeSelf)
        {
            eventSystem.SetActive(false);
        }
        else
        {
            eventSystem.SetActive(true);
        }
    }
}
