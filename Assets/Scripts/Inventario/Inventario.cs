﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Inventory", menuName = "ScriptableObjects/InventarioScriptableObject", order = 3)]
public class Inventario : ScriptableObject
{
    public List<Items> itemsInInventory = new List<Items>();
    [System.Serializable]
    public class Items
    {
        public ItemInventario item;
        public int amount;        
    }
    private void OnEnable()
    {
        for (int i = 0; i < itemsInInventory.Count; i++)
        {
            itemsInInventory[i].amount = 20;
        }
    }
}
