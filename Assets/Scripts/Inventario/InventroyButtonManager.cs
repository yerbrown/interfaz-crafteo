﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InventroyButtonManager : MonoBehaviour
{
    public Inventario inventory;
    public List<Button> buttons;
    public GameObject standarButton;
    private void OnEnable()
    {
        for (int i = 0; i < inventory.itemsInInventory.Count; i++)
        {
            ItemButton boton = buttons[i].GetComponent<ItemButton>();
            boton.itemInBotton = inventory.itemsInInventory[i];
            boton.UpdateButtonUI();
        }
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            for (int i = 0; i < inventory.itemsInInventory.Count; i++)
            {
                inventory.itemsInInventory[i].amount++;
                buttons[i].GetComponent<ItemButton>().UpdateButtonUI();
            }
        }

    }
}
