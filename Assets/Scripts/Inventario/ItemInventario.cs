﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Item", menuName = "ScriptableObjects/ItemInventarioScriptableObject", order = 4)]
public class ItemInventario : ScriptableObject
{
    public string itemName;
    public Sprite itemSpr;
}
