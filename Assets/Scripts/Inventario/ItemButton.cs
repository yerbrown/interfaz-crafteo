﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemButton : MonoBehaviour
{
    public Inventario.Items itemInBotton;
    [Header("UI")]
    public Image buttonImg;
    public Text amountText;
    // Start is called before the first frame update
    private void Awake()
    {
        if (itemInBotton==null)
        {
            //nameText.gameObject.SetActive(false);
        }
    }
    public void UpdateButtonUI()
    {
        //nameText.gameObject.SetActive(true);
        buttonImg.sprite = itemInBotton.item.itemSpr;
        amountText.text = itemInBotton.amount.ToString();
    }
    public void SumAmount()
    {
        itemInBotton.amount++;
        UpdateButtonUI();
    }
}
