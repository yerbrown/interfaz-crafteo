﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class RecipeButton : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public RecipesUI recipe;
    [Header("UI")]
    public GameObject buttonRecipe;
    public Text recipeName;
    public Text level;
    public Text amount;
    public Image objectToCraft;
    public GameObject checker;
    public GameObject block;
    void Start()
    {
        InitialiceButton();
    }

    public void InitialiceButton()
    {
        if (recipe != null)
        {
            level.text = "LVL: " + recipe.level.ToString();
            amount.text = recipe.amount.ToString();
            objectToCraft.sprite = recipe.objectToCreate;
            block.SetActive(!recipe.unlocked);
            UnlockRecipes();
            if (recipe.unlocked == false)
            {
                objectToCraft.color = Color.black;
                GetComponent<Button>().interactable = false;
                block.SetActive(true);
                checker.SetActive(false);
            }
            else
            {
                objectToCraft.color = Color.white;
                GetComponent<Button>().interactable = true;
                block.SetActive(false);
                UpdateButton();
            }
        }
        else
        {
            GetComponent<Button>().interactable = false;
            buttonRecipe.SetActive(false);
        }
    }
    public void ShowItemScraft()
    {
        Inventory.inventoryInstance.selectedCraftList = recipe.ListOfItemNeeded;
        Inventory.inventoryInstance.selectedCraft = this;
        Inventory.inventoryInstance.craftButton.interactable = true;
        Inventory.inventoryInstance.ShowCraftRequired(Inventory.inventoryInstance.selectedCraftList);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        Selected();
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        Unselected();
    }
    public void ReadyToCraft()
    {
        for (int i = 0; i < recipe.ListOfItemNeeded.Count; i++)
        {
            if (recipe.ListOfItemNeeded[i].Item != null)
            {
                if (recipe.ListOfItemNeeded[i].Item.amount >= recipe.ListOfItemNeeded[i].ItemAmountNeeded)
                {
                    recipe.readyToCraft = true;
                }
                else
                {
                    recipe.readyToCraft = false;
                    break;
                }
            }
        }
    }
    public void UnlockRecipes()
    {
        if (Inventory.inventoryInstance.level >= recipe.level)
        {
            recipe.unlocked = true;
        }
        else
        {
            recipe.unlocked = false;
        }
    }
    public void Selected()
    {
        if (recipe != null && recipe.unlocked)
        {
            Inventory.inventoryInstance.ShowCraftRequired(recipe.ListOfItemNeeded);
        }
    }
    public void Unselected()
    {
        Inventory.inventoryInstance.ShowCraftRequired(Inventory.inventoryInstance.selectedCraftList);
    }
    public void UpdateButton()
    {
        amount.text = recipe.amount.ToString();
        ReadyToCraft();
        if (recipe.readyToCraft)
        {
            checker.SetActive(true);
        }
        else
        {
            checker.SetActive(false);
        }
    }
}
